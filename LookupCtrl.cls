/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 12-30-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
**/
public with sharing class LookupCtrl {
    @AuraEnabled(cacheable=true)
    public static List < SObjectResult > getSingleLookupRecords( String searchTerm, string myObject, String filter) {
        String myQuery = null;
            if(filter != null && filter != ''){
                myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' AND '+filter+' LIMIT  50000';
            } else {
                if(searchTerm == null || searchTerm == ''){
                    myQuery = 'Select Id, Name from '+myObject+' Where LastViewedDate != NULL ORDER BY LastViewedDate DESC LIMIT  50000';
                }
                else {
                    myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' LIMIT  5';
                }
            }
        
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();

        for( sObject so : Database.Query(myQuery) ) {
                String name = (String)so.get('Name');
                sObjectResultList.add(new SObjectResult(name, so.Id));
            }

        return sObjectResultList;
    }
    public class SObjectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String recNameTemp, Id recIdTemp) {
            recName = recNameTemp;
            recId = recIdTemp;
        }
    }
}