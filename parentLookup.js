import { LightningElement ,track,api} from 'lwc';

export default class ParentLookup extends LightningElement {

    @api selectedId;
    @track objectName='User';
    handleAccountSelection(event) {
        this.selectedId = event.detail;
    }
}