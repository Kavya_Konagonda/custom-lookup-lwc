import { LightningElement,api,track } from 'lwc';
import getSingleLookupRecords from '@salesforce/apex/LookuoCtrl.getSingleLookupRecords';

export default class Lookup extends LightningElement {
    @api objName;
    @api iconName;
    @api filter = '';
    @api label;
    @api accessLevel = '';
    @api searchPlaceholder = 'Search';
    
    @track selectedName;
    @track records;
    @track isValueSelected;
    @track blurTimeout;
    @track ceateNewButtonLabel = '';

    searchTerm;
    //css
    @track boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    @track inputClass = '';

    getRecords() {

            getSingleLookupRecords({
                searchTerm: this.searchTerm,
                myObject: this.objName,
                filter: this.filter,
                accessLevel: this.accessLevel,
                isFromAccessRequest: true,
            }).then(resp => {
                this.error = undefined;
                this.records = resp;
            }).catch(err => {
                this.error = err;
                this.records = undefined;
            });
    }

    handleClick() {
        this.searchTerm = '';
        this.inputClass = 'slds-has-focus';
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus slds-is-open';
    }

    onBlur() {
        this.blurTimeout = setTimeout(() => { this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus' }, 300);
        this.searchTerm = '';
    }

    onSelect(event) {
        let selectedId = event.currentTarget.dataset.id;
        let selectedName = event.currentTarget.dataset.name;
        const valueSelectedEvent = new CustomEvent('lookupselected', { detail: selectedId });
        this.dispatchEvent(valueSelectedEvent);
        this.isValueSelected = true;
        this.selectedName = selectedName;
        if (this.blurTimeout) {
            clearTimeout(this.blurTimeout);
        }
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    }

    handleRemovePill() {
        this.isValueSelected = false;
        const valueSelectedEvent = new CustomEvent('lookupselected', { detail: '' });
        this.dispatchEvent(valueSelectedEvent);
        this.searchTerm = '';
    }

    onChange(event) {
        this.searchTerm = event.target.value;
        this.getRecords();
    }
}